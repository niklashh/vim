(use-package markdown-mode
  :ensure
  :config (add-to-list 'auto-mode-alist '("\\.mdx?\\'" . markdown-mode)))

(provide 'init-markdown)
