(use-package tex-mode
  :ensure auctex)

;; (use-package auctex-latexmk
;;   :ensure
;;   :config
;;   (auctex-latexmk-setup))

(provide 'init-tex)
