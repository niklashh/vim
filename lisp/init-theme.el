(use-package dracula-theme
  :ensure
  :init
  (load-theme 'dracula t))

(custom-set-faces
 '(lsp-face-semhl-static ((t nil))) ; static highlighting breaks function-like methods
 '(lsp-face-semhl-macro ((t nil))) ; macro highlighting breaks async/await keywords
 '(lsp-face-semhl-variable ((t (:inherit font-lock-variable-name-face :slant normal :weight normal))))
 '(font-lock-function-name-face ((t (:foreground "#17D5D9" :weight normal))))
 '(font-lock-keyword-face ((t (:foreground "#D7068A" :weight bold))))
 '(font-lock-preprocessor-face ((t (:foreground "#10989B" :weight normal))))
 '(font-lock-variable-name-face ((t (:foreground "#f8f8f2" :weight normal :slant normal)))))

(provide 'init-theme)
