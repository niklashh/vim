(use-package zig-mode
  :ensure
  :mode "\\.zig\\'")

(provide 'init-zig)
