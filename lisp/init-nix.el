(use-package nix-mode
  :mode "\\.nix\\'")

(provide 'init-nix)
