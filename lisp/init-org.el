(add-hook 'org-mode-hook
          (lambda ()
            (require 'ox-latex)
            (add-to-list 'org-latex-classes
                         '("apa7"
                           "\\documentclass{apa7}"
                           ("\\section{%s}" . "\\section*{%s}")
                           ("\\subsection{%s}" . "\\subsection*{%s}")
                           ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                           ("\\paragraph{%s}" . "\\paragraph*{%s}")
                           ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
            (org-indent-mode 1)))

(font-lock-add-keywords 'org-mode
  '(("^ +\\([-*]\\) "
     (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "∙"))))))

(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process '("latexmk -pdflatex='%latex -interaction=nonstopmode -shell-escape' -pdf -bibtex -f -output-directory=%o %f"))

(provide 'init-org)
