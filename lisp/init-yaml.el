(use-package yaml-mode
  :ensure
  :config
  (add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-mode)))

(provide 'init-yaml)
