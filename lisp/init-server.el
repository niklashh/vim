
  ;; (setenv "PATH" (concat (getenv "PATH") ":~/.local/bin"))
  ;; (setq exec-path (append exec-path '("~/.local/bin")))
;; fix emacs server path
(when (daemonp)
  (use-package exec-path-from-shell
    :ensure
    :init
    (exec-path-from-shell-initialize)
    :custom
    (exec-path-from-shell-arguments nil)))

(provide 'init-server)
