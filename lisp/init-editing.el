(use-package rainbow-delimiters :ensure)

(use-package smex
  :ensure
  :commands smex
  :config (smex-initialize)
  :bind ("M-x" . smex))

(use-package flx-ido
  :ensure
  :config
  (ido-mode 1)
  (ido-everywhere 1)
  (flx-ido-mode 1)
  :custom
  (ido-enable-flex-matching t) ;; disable ido faces to see flx highlights.
  (ido-use-faces nil))

(use-package undo-tree
  :ensure
  :config
  (global-undo-tree-mode 1)
  :custom
  (undo-tree-auto-save-history t)
  (undo-tree-visualizer-timestamps 1))

(use-package ace-window
  :ensure
  :commands ace-window
  :bind
  ("M-o" . ace-window)
  :custom
  (aw-dispatch-always t))

(use-package embrace
  :ensure
  :bind
  ("M-s" . embrace-commander))

(use-package multiple-cursors
  :ensure
  :bind
  ("ESC e" . mc/mark-next-like-this-word)
  ("ESC E" . mc/edit-lines))

(use-package move-text
  :ensure
  :bind
  ("<M-up>" . move-text-up)
  ("<M-down>" . move-text-down))

;; debug keypresses with `view-lossage'

(use-package visual-fill-column
  :ensure
  :config
  (global-visual-fill-column-mode))

(use-package darkroom
  :ensure)

;;------------------------------;;

(add-hook 'prog-mode-hook (lambda ()
			    (rainbow-delimiters-mode 1)
			    (electric-pair-mode 1)))

;; Save cursor location for each file
(save-place-mode 1)

;; Highlights matching parenthesis
(show-paren-mode 1)

;; Word/line wrap
(global-visual-line-mode 1)

;; Show column next to line
(setq column-number-mode t)

(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)
(global-set-key (kbd "M-RET") 'default-indent-new-line)
(global-set-key (kbd "M-%") 'query-replace-regexp)
(global-set-key (kbd "ESC a") 'align-comment)

(setq scroll-margin 5)

(setq scroll-step 1) ;; keyboard scroll one line at a time)

;; Don't do auto-saving of every file-visiting buffer
(setq auto-save-default nil)
(setq-default indent-tabs-mode nil)

(defun align-comment ()
  (interactive)
  (align-regexp (region-beginning) (region-end) "\\(\\s-*\\)\\(//\\)" 1 1 nil))

(defun toggle-comment-on-line ()
  "Comment or uncomment current line."
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))
(global-set-key (kbd "C-j") 'toggle-comment-on-line) ; HACK: alacritty config remaps C-; -> C-j

;; (setq wl-copy-process nil)
;; (defun wl-copy (text)
;;   (setq wl-copy-process (make-process :name "wl-copy"
;;                                       :buffer nil
;;                                       :command '("wl-copy" "-f" "-n")
;;                                       :connection-type 'pipe))
;;   (process-send-string wl-copy-process text)
;;   (process-send-eof wl-copy-process))
;; (defun wl-paste ()
;;   (if (and wl-copy-process (process-live-p wl-copy-process))
;;       nil ; should return nil if we're the current paste owner
;;     (shell-command-to-string "wl-paste -n")))
;; (setq interprogram-cut-function 'wl-copy)
;; (setq interprogram-paste-function 'wl-paste)

;; The above doesn't work "Failed to connect to a Wayland server"

(provide 'init-editing)
