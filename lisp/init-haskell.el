(use-package haskell-mode
  :ensure
  :mode "\\.hs\\'")

(use-package lsp-mode ;; TODO
  :ensure
  :commands lsp
  :custom
  (lsp-idle-delay 0.0) ; TODO what does this do
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-eldoc-enable-hover nil)
  (lsp-enable-snippet nil)
  (lsp-signature-render-documentation nil)
  (lsp-enable-semantic-highlighting t) ; enables syntax highlight in doc comments
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

(use-package lsp-haskell
  :ensure
  :config
  (add-hook 'haskell-mode-hook #'lsp)
  (add-hook 'haskell-literate-mode-hook #'lsp))

(provide 'init-haskell)
