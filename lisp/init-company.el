(use-package company
  :ensure
  :config
  (company-tng-configure-default)
  :custom
  (company-idle-delay 0) ;; how long to wait until popup
  (company-dabbrev-downcase 0)
  (company-tooltip-align-annotations t)
  ;; (company-begin-commands nil) ;; uncomment to disable popup
  :bind
  (:map company-active-map
	      ("TAB". company-select-next)
	      ("<backtab>". company-select-previous)
	      ("C-p". nil)
	      ("C-n". nil)))

(provide 'init-company)
