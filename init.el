;; Change the user-emacs-directory to keep unwanted things out of ~/.emacs.d
(setq user-emacs-directory (expand-file-name "~/.cache/emacs/")
      url-history-file (expand-file-name "url/history" user-emacs-directory))

;; Keep customization settings in a temporary file
(setq custom-file
      (if (boundp 'server-socket-dir)
          (expand-file-name "custom.el" server-socket-dir)
        (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))
(load custom-file t)

;; Define and initialize package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; use-package to simplify the config file
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure 't)

;; Keyboard-centric user interface
(setq inhibit-startup-message t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(defalias 'yes-or-no-p 'y-or-n-p)

;; Auto save directory
(setq backup-directory-alist
      `((".*" . ,(expand-file-name "saves" user-emacs-directory))))
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name "saves" user-emacs-directory) t)))

;; Use no-littering to automatically set common paths to the new user-emacs-directory
(use-package no-littering)

(add-to-list 'load-path "~/.emacs.d/lisp")

(require 'init-editing)
(require 'init-server)
(require 'init-company)
(require 'init-markdown)
(require 'init-rust)
(require 'init-haskell)
(require 'init-glsl)
(require 'init-yaml)
(require 'init-theme)
(require 'init-magit)
(require 'init-org)
(require 'init-idris)
(require 'init-scala)
(require 'init-tex)
(require 'init-lean)
(require 'init-zig)
(require 'init-nix)

;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

;; Profile emacs startup
(add-hook 'emacs-startup-hook
	  (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))
